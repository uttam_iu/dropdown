//
//  ViewController.swift
//  DropDown
//
//  Created by Syncrhonous on 29/4/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var fruitList = ["Afghanistan", "Albania", "Algeria", "Australia", "Bahamas", "Bangladesh", "China", "India", "Japan", "Irak", "Afghanistan", "Albania", "Algeria", "Australia", "Bahamas", "Bangladesh", "China", "India", "Japan", "Irak","Afghanistan", "Albania", "Algeria", "Australia", "Bahamas", "Bangladesh", "China", "India", "Japan", "Irak","Afghanistan", "Albania", "Algeria", "Australia", "Bahamas", "Bangladesh", "China", "India", "Japan", "Irak"]
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnDrop: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if tblView.isHidden {
            animate(toogle: true, type: btnDrop)
        } else {
            animate(toogle: false, type: btnDrop)
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func btnDropClicked(_ sender: UIButton) {
        
        if tblView.isHidden {
            animate(toogle: true, type: btnDrop)
        } else {
            animate(toogle: false, type: btnDrop)
        }
    }
    
    
    func animate(toogle: Bool, type: UIButton) {
        
        if type == btnDrop {
            
            if toogle {
                UIView.animate(withDuration: 0.3) {
                    self.tblView.isHidden = false
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    self.tblView.isHidden = true
                }
            }
        } else {
            if toogle {
                UIView.animate(withDuration: 0.3) {
                    //self.lbl.isHidden = false
                }
            } else {
                UIView.animate(withDuration: 0.3) {
                    //self.lbl.isHidden = true
                }
            }
        }
    }
    
}



extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fruitList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = fruitList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        btnDrop.setTitle("\(fruitList[indexPath.row])", for: .normal)
        animate(toogle: false, type: btnDrop)
    }
}
